
## Getting started

Compile with `cargo build`. Build & run with `cargo run`.

## Libraries used

| library         | homepage                                             | docs                                      |
| :-------------: | :--------------------------------------------------: | :---------------------------------------: |
| piston          | http://www.piston.rs/                                | http://docs.piston.rs/                    |
| hyper           | http://hyper.rs/                                     | http://hyper.rs/hyper/                    |
| rustc-serialize | https://github.com/rust-lang-nursery/rustc-serialize | https://doc.rust-lang.org/rustc-serialize |
