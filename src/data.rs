
pub struct StoryNode<'a> {
  pub dialogue: Vec<Dialogue<'a>>,
  pub choices: Vec<Choice<'a>>
}

pub struct Character<'a> {
  pub name: &'a str,
  pub pronoun_she: &'a str,
  pub pronoun_her: &'a str,
  pub pronoun_hers: &'a str,
}

pub struct Choice<'a> {
  pub description: &'a str,
  pub result: &'a StoryNode<'a>
}

pub struct Dialogue<'a> {
  pub speaker: &'a Character<'a>,
  pub words: &'a str,
}
