extern crate hyper;
extern crate hyper_native_tls;
extern crate rustc_serialize;
extern crate piston_window;
extern crate tachikoma;

// third party
use hyper::Client;
use hyper::net::HttpsConnector;
use hyper_native_tls::NativeTlsClient;
use rustc_serialize::json::Json;
use piston_window::*;

// stdlib
use std::io::Read;

use tachikoma::data::*;

fn main() {
  data();
  //curl();
}

fn data() {
  let name: &'static str = "A";
  let she: &'static str = "she";
  let her: &'static str = "her";
  let hers: &'static str = "hers";
  let player = Character {
    name: name,
    pronoun_she: she,
    pronoun_her: her,
    pronoun_hers: hers
  };

  println!("{} is a human, {} name is {}. That is {}", player.pronoun_she, player.pronoun_her, player.name, player.pronoun_hers);
}

// Read data from a URL and print it to the screen.
fn curl() {

  let ssl = NativeTlsClient::new().unwrap();
  let connector = HttpsConnector::new(ssl);
  let client = Client::with_connector(connector);
  let mut s = String::new();

  let res = client
    .get("https://poloniex.com/public?command=returnTicker")
    .send()
    .unwrap()
    .read_to_string(&mut s);

  let data = Json::from_str(s.as_str()).unwrap();

  for (k, v) in data.as_object().unwrap().iter() {
    println!("{}: {}", k, match *v {
      Json::U64(v) => format!("{} (u64)", v),
      Json::String(ref v) => format!("{} (string)", v),
      Json::Object(ref v) => format!("{:?} (obj)", v),
      _ => format!("other")
    });
  }

  println!("{}", data);

  let mut window: PistonWindow = WindowSettings::new("Hello World", [640, 480])
    .exit_on_esc(true)
    .build()
    .unwrap();

  while let Some(e) = window.next() {
    window.draw_2d(&e, |c, g| {
      clear([1.0; 4], g);
      rectangle([1.0, 0.0, 0.0, 1.0], // red
                [0.0, 0.0, 100.0, 100.0],
                c.transform, g);
    });
  }

}
